import { createStackNavigator, createSwitchNavigator } from 'react-navigation';
import SignUp from './src/pages/SignUp/signup';
import LogIn from './src/pages/LogIn/login';
import Chat from './src/pages/chat/chat';

var signupStack = createStackNavigator({
  LoginScreen: LogIn,
  SignUpScreen: SignUp
});

var chatStack = createStackNavigator({
  ChatScreen: Chat
});

var rootNavigation = createSwitchNavigator({
  ChatStack: chatStack,
  SignupStack: signupStack
})

export default rootNavigation;