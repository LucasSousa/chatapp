import * as firebase from 'firebase';
import { errorMessageService } from "./error-message";

// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyClHvGcCQceopDHyVsDnIIm56FZO15PuY8",
    authDomain: "mychat-df3f0.firebaseapp.com",
    databaseURL: "https://mychat-df3f0.firebaseio.com",
    projectId: "mychat-df3f0",
    storageBucket: "mychat-df3f0.appspot.com",
    messagingSenderId: "785963474083"
};

firebase.initializeApp(firebaseConfig);

export const userService = {

    signUp: (userModel) => {
        return new Promise((resolve, reject) => {
            firebase.auth().createUserAndRetrieveDataWithEmailAndPassword(
                userModel.email,
                userModel.password
            ).then(credential => { // Promisse executou com sucesso
                resolve(credential);
            }).catch(error => { // Ocorreu erro no processamento
                var messageToShow = "";
                console.log(error.message);
                switch (error.message) {
                    case "The email address is badly formatted.":
                        messageToShow = errorMessageService
                                            .getErrorByName("invalid_email");
                        break;
                    case "Password should be at least 6 characters":
                        messageToShow = errorMessageService
                                        .getErrorByName("password_min_6_digits");
                        break;
                    default: 
                        messageToShow = "Ocorreu um erro inesperado.";
                        break;
                }
                reject(messageToShow);
            });
        });
    },

    login: (email, password) => {
        return new Promise((resolve, reject) => {
            firebase.auth()
                .signInWithEmailAndPassword(email, password)
                    .then(credentials => {
                        resolve(credentials)
                    }).catch(error => {
                        reject(error);
                    });
        })
    }

}